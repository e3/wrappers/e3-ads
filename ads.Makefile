#
#  Copyright (c) 2018 - 2019  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Jeong Han Lee
# email   : jeonghan.lee@gmail.com
# Date    : Saturday, October  5 01:44:11 CEST 2019
# version : 0.0.1
#
## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

APP:=adsApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

APPEX:=adsExApp
APPEXDB:=$(APPEX)/Db

CXXFLAGS += -std=c++11

USR_INCLUDES += -I$(where_am_I)$(APPSRC)

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.proto)
TEMPLATES += $(wildcard $(APPDB)/*.template)

TEMPLATES += $(wildcard $(APPEXDB)/*.db)
TEMPLATES += $(wildcard $(APPEXDB)/*.proto)
TEMPLATES += $(wildcard $(APPEXDB)/*.template)

HEADERS += $(APPSRC)/adsAsynPortDriver.h
HEADERS += $(APPSRC)/adsAsynPortDriverUtils.h

SOURCES += $(APPSRC)/adsAsynPortDriver.cpp
SOURCES += $(APPSRC)/adsAsynPortDriverUtils.cpp
DBDS    += $(APPSRC)/ads.dbd


SCRIPTS += $(wildcard ../iocsh/*.iocsh)

.PHONY: 

#
USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)
#
SUBS=$(wildcard $(APPDB)/*.substitutions)
TMPS=$(wildcard $(APPDB)/*.template)
#
#

vlibs: 

.PHONY: vlibs



