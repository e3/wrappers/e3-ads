
e3-ads  
======
ESS Site-specific EPICS module that provides an asyn port implementation for communication using the ADS protocol by Beckhoff.

Build instructions
==================
To build and install the module in a local e3 environment:

```bash
$ make init
$ make build
$ make install
```

To build and install in cell mode for e3:

```bash
$ make init
$ make cellbuild
$ make cellinstall
```

Usage
=====

To use the module declare a require at the start of your startup script:

```
require ads
```

Define asyn port
================

To communicate using the module you need to declare an asyn port for your Beckhoff PLC in your startup script

```
############# Configure ads device driver:
# 1. Asyn port name                         :  "ADS_1"
# 2. IP                                     :  "192.168.88.44"
# 3. AMS of plc                             :  "192.168.88.44.1.1"
# 4. Default ams port                       :  851 for plc 1, 852 plc 2 ...
# 5. Parameter table size (max parameters)  :  1000
# 6. priority                               :  0
# 7. disable auto connnect                  :  0 (autoconnect enabled)
# 8. default sample time ms                 :  50
# 9. max delay time ms (buffer time in plc) :  100
# 10. ADS command timeout in ms             :  5000 
# 11. default time source (PLC=0,EPICS=1)   :  0 (PLC) NOTE: record TSE field need to be set to -2 for timestamp in asyn ("field(TSE, -2)")

adsAsynPortDriverConfigure("ADS_1","192.168.88.63","5.81.143.110.1.1", 852, 1000, 0, 0, 50, 100, 5000, 0)
```

Database
========

Simple record below using the ADS\_1 port declared above. This record will connect the EL6688_PTP.bEL6688FullySynched node on your Beckhoff PLC to an analogue input record in your EPICS database.

For further details see [epic-twincat-ads](https://github.com/epics-modules/epics-twincat-ads) documentation () and [examples](https://github.com/epics-modules/epics-twincat-ads/tree/master/adsExApp).

```
record(ai,"MySystem:MyProcessValue"){
  field(DTYP, "asynInt32")
  field(INP,  "@asyn(ADS_1,0,1)ADSPORT=852/EL6688_PTP.bEL6688FullySynched?")
  field(SCAN, "I/O Intr")
}
```
